using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DatosJugador : MonoBehaviour
{
    public int vidaPlayer;
    public Slider vidaVisual;
    public GameObject gameOverText;

    
    // Update is called once per frame
    void Update()
    {
        if (vidaPlayer <= 0)
        {
            vidaVisual.GetComponent<Slider>().value = vidaPlayer;

            if (vidaPlayer <= 0)

                vidaVisual.value = vidaPlayer;

            if (!gameOverText.activeSelf)
            {
                gameOverText.SetActive(true);
            }
            {

                Debug.Log("GameOverText");
            }
        }
    }
}
