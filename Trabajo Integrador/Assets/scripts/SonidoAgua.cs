using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonidoAgua : MonoBehaviour
{
    public AudioClip sonidoAgua; // Clip de sonido que se reproducirá
    private AudioSource audioSource; // Referencia al AudioSource

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }
        audioSource.clip = sonidoAgua;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Agua"))
        {
            if (audioSource != null && !audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }
    }
void OnTriggerExit(Collider other)
{
    if (other.CompareTag("Agua"))
    {
        if (audioSource != null && audioSource.isPlaying)
        {
            audioSource.Stop();
        }
    }
}
}

