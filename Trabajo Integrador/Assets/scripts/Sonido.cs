using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sonido : MonoBehaviour
{
    public AudioClip soundClip; // Referencia al clip de sonido
    private AudioSource audioSource; // Componente AudioSource para reproducir el sonido

    private bool _isOpen = false;

    void Start()
    {
        // Obtener el componente AudioSource o agregar uno si no existe
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }

        // Asignar el clip de sonido al AudioSource
        audioSource.clip = soundClip;
    }

    public void ToggleDoor()
    {
        _isOpen = !_isOpen;

        if (_isOpen)
        {
            // Si la puerta se abre, reproducir el sonido
            if (audioSource != null && soundClip != null)
            {
                audioSource.Play();
            }
        }
    }
}

