using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoDamage : MonoBehaviour
{
    public int damage;
    public GameObject Player;
    public GameObject gameOverText; // Referencia al objeto de texto de "GAME OVER"

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Player.GetComponent<DatosJugador>().vidaPlayer -= damage;

            if (Player.GetComponent<DatosJugador>().vidaPlayer <= 0)
            {
                if (gameOverText != null)
                {
                    gameOverText.SetActive(true); // Mostrar el cartel de "GAME OVER"
                }

                // Aqu� puedes pausar el juego, reiniciar la escena, etc.
            }
        }

        if (other.CompareTag("Enemigo"))
        {
            Debug.Log("Esto es un enemigo");
        }
    }
}
