using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    public Transform door;
    public Transform openTransform;
    public Transform closedTransform;
   
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            door.position = openTransform.position;

        }
    }
        

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            door.position = closedTransform.position;
        }
    }
}
