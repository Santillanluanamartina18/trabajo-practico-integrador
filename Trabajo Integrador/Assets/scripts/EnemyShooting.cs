using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    public Transform player; // Referencia al transform del jugador
    public GameObject bulletPrefab; // Prefab de la bala
    public float fireRate = 1f; // Frecuencia de disparo
    public float bulletSpeed = 10f; // Velocidad de la bala

    private float nextTimeToFire = 0f;

    void Update()
    {
        // Verificar si es tiempo de disparar
        if (Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            ShootAtPlayer();
        }
    }

    void ShootAtPlayer()
    {
        // Calcular la direcci�n hacia el jugador
        Vector3 targetDirection = (player.position - transform.position).normalized;

        // Rotar hacia la direcci�n del jugador (opcional, depende de la orientaci�n del enemigo)
        transform.forward = targetDirection;

        // Instanciar una bala
        GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity);

        // Obtener el componente Rigidbody de la bala (asumiendo que la bala tiene un Rigidbody)
        Rigidbody bulletRb = bullet.GetComponent<Rigidbody>();

        // Asignar velocidad a la bala en la direcci�n del jugador
        bulletRb.velocity = targetDirection * bulletSpeed;
    }
}

