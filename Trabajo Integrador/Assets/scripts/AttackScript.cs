using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackScript : MonoBehaviour
{
    public int liveCost = 1;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {

            other.GetComponent<PlayerHealth>().ReceiveDamage(liveCost);

            GetComponentInParent<EnemyScriot>().KillMe();

        }
    }
}
    
    
 
