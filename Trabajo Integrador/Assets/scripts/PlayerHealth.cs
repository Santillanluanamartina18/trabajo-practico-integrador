using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public int lives = 3;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

     public void ReceiveDamage(int liveCost) {
        lives -= liveCost;

        if (lives < 0) {
            Death();
        }
    }

    void Death() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}





