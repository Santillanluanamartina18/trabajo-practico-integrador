using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.VisualScripting;

public class PlayerController : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    Rigidbody Rb;
    public float fuerzaSalto = 10f;
    public int maxSaltos = 2;
    public int saltosRealizados = 0;
    private bool puedeSaltar = true;

    public GameObject Spheere;
    public GameObject Corazon5;
    public GameObject cubolava;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Rb = GetComponent<Rigidbody>();
    }
    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;

        }

        if (Input.GetButtonDown("Jump") && (saltosRealizados < maxSaltos || puedeSaltar))
        {
            Saltar();
        }

        void Saltar()
        {
            if (saltosRealizados < maxSaltos)
            {
                Rb.velocity = new Vector3(Rb.velocity.x, 0, Rb.velocity.z);
                Rb.AddForce(Vector3.up * fuerzaSalto, ForceMode.Impulse);
                saltosRealizados++;

                if (saltosRealizados == 1)
                {
                    puedeSaltar = true;
                }
            }

        }
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Corazon 5")
        {
            SceneManager.LoadScene("You Win");
        }


        if (collision.gameObject.tag == "Sphere")

        {

            rapidezDesplazamiento = 25.0f;

            Spheere.SetActive(false);
            this.transform.localScale = new Vector3(5, 5, 5);
        }

        if (collision.gameObject.tag == "cubo lava")
        {
            SceneManager.LoadScene("escena 1");
        }
    }

        
            
               
    











}
               

   

 