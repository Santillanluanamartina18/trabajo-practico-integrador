using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorPuerta : MonoBehaviour
{
    public float anguloMaximo = 90.0f; // El �ngulo m�ximo que la puerta puede rotar
    public float velocidadRotacion = 2.0f; // La velocidad de rotaci�n de la puerta

    private Quaternion rotacionInicial;
    private Quaternion rotacionAbierta;
    private bool abriendo = false;
    private bool cerrando = false;

    void Start()
    {
        rotacionInicial = transform.rotation;
        rotacionAbierta = Quaternion.Euler(0, anguloMaximo, 0) * rotacionInicial;
    }

    void Update()
    {
        if (abriendo)
        {
            AbrirPuerta();
        }
        else if (cerrando)
        {
            CerrarPuerta();
        }
    }

    public void Abrir()
    {
        abriendo = true;
        cerrando = false;
    }

    public void Cerrar()
    {
        cerrando = true;
        abriendo = false;
    }

    void AbrirPuerta()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, rotacionAbierta, velocidadRotacion * Time.deltaTime);

        // Si la puerta ha alcanzado su �ngulo m�ximo, detenemos el proceso de apertura
        if (Quaternion.Angle(transform.rotation, rotacionAbierta) < 0.01f)
        {
            transform.rotation = rotacionAbierta;
            abriendo = false;
        }
    }

    void CerrarPuerta()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, rotacionInicial, velocidadRotacion * Time.deltaTime);

        // Si la puerta ha alcanzado su posici�n inicial, detenemos el proceso de cierre
        if (Quaternion.Angle(transform.rotation, rotacionInicial) < 0.01f)
        {
            transform.rotation = rotacionInicial;
            cerrando = false;
        }
    }
}
