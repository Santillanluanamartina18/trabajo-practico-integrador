using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder.MeshOperations;

public class EnemigoSeguidor : MonoBehaviour
{
    public Transform objetivo;
    public float velocidad = 5f;
    public float distanciaMinimaAtaque = 3f;
    public LayerMask capasObstaculos;
    void Update()
    {
        if (objetivo != null)
        {
            float distancia = Vector3.Distance(transform.position, objetivo.position);

            if (distancia > distanciaMinimaAtaque)

                transform.LookAt(objetivo);
            transform.Translate(Vector3.forward * velocidad * Time.deltaTime);
        }

        else
        {
            {
                // Realizar un raycast hacia el jugador para verificar obst�culos
                RaycastHit hit;
                Vector3 direccion = objetivo.position - transform.position;

                if (Physics.Raycast(transform.position, direccion, out hit, distanciaMinimaAtaque, capasObstaculos))
                {
                    // Si el raycast golpea un obst�culo antes del jugador, no se realiza el ataque
                    Debug.Log("�Obst�culo entre el enemigo y el jugador!");
                }
                else
                {
                    // Atacar al jugador si no hay obst�culos
                    Atacar();
                }
            }
        }
    }

    void Atacar()
    {
        // Coloca aqu� la l�gica de ataque al jugador
        // Por ejemplo:
        Debug.Log("�Atacando al jugador!");
        // Aqu� puedes llamar a funciones para da�ar al jugador o iniciar una animaci�n de ataque, etc.
    }
}