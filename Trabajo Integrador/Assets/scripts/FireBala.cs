using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firebala : MonoBehaviour
{
    [SerializeField]
    public GameObject _bala;
    [SerializeField]
    private float _timer = 2f;
    private float timerCount = 0f;

    
    private int _counter;
    [SerializeField]
    private int _maxCounter = 50;
    void Start()
    {
        StartCoroutine(Firebala_CR());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator Firebala_CR()
    {
        Debug.Log("inicio coroutine");
        for (int i = 0; i < _maxCounter; i++)
        {
            _counter++;
            Instantiate(_bala, transform.position, transform.rotation);
            yield return new WaitForSeconds(_timer);
        }
        Debug.Log("Fin coroutine");
    }
}
