using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyScriot : MonoBehaviour
{
    NavMeshAgent agent;
    Transform target;
    public float distanceToPursuit = 5f;
    public Transform[] waypoints;
    int currentWaypoint = 0;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, target.position) < distanceToPursuit)
        {

            agent.destination = target.transform.position;
        }
        else
        {

            if (!agent.hasPath)
            {

                if (currentWaypoint < waypoints.Length - 1)
                {
                    currentWaypoint++;
                }
                else
                {
                    currentWaypoint = 0;
                }

                agent.destination = waypoints[currentWaypoint].position;
            }
        }
    }

    public void KillMe()
    {
        Destroy(gameObject);
    }
}
        